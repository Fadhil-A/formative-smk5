public class Test {
    public static void main(String[] args) { //kode utama yang akan dijalankan
        Test obj = new Test();//ini membuat object baru dari class Test dengan nama obj
        obj.start();//ini yang akan dijalankan oleh java, pertama Object object akan menjalankan method start
    }
    public void start() {//ini yang dijalankan oleh Object obj
        String stra = "do"; //membuat variabel stra dengan value "do"
        String strb = method(stra);//membuat variabel strb dan memasukkan variabel stra di Method method dengan nilai stra yang berarti stra bisa diinput
        System.out.print(": "+stra + strb);
    }
    public String method(String stra) {//method ini dijalankan oleh method start
        stra = stra + "good";//disini variabel stra didefinisikan dngan variabel stra yang ada di parameter ditambah kata "good"
        System.out.print(stra);//ini print variabel stra
        return " good";//sekaligus ditambah dengan kata " good" yang diberi oleh return
    }
}
	// jadi dari main menjalankan obj.start()|Object obj menjalankan method start dan akan meneruskan nilai stra yaitu "do" kedalam print pada method start
	// lalu akan mengeprint variabel stra + strb|value nya yaitu stra = "do", strb mengambil dari Method method nilai dari parameter milik method yaitu (stra)|
	// nillai stra tsb diambil dari parameter Method 'method'  yang diset valuenya yaitu "stra" = 'stra' + "good"| 
	//	"stra merupakan parameter" 'stra merupakan variabel milik method start() yaitu valuenya "do"
	// maka ketika Method method dijalankan akan melakukan print valuenya "dogood"
	//nah hasil output akhir yaitu "dogood:dogood"| kenapa diakhir ada "dogood" padahal di comment diatas hanya " good"?
	//karena dari method start() print stra+strb hasilnya "dogood" karena nilai stra di method start() adalah do, strb berdasarkan return Method method yaitu " good"

