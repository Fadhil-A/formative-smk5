interface Car{
public void tipe();
public String mesin = "6 Silinder";
public int roda = 0;
public int kpenumpang = 0;
}

class Bis implements Car{
	int roda = 6;
	int kpenumpang = 20;
	public void tipe(){
	System.out.println("--- Bis ---");
	System.out.println("- Mesin Bis ini adalah : " + mesin);
	System.out.println("- Roda bis ini ada : " + roda);
	System.out.println("- Kapasitas penumpang di bis ini adalah : " + kpenumpang);
	}
}
class Mobil implements Car{
	int roda = 4	;
	int kpenumpang = 4;
	public void tipe(){
	System.out.println("--- Mobil Pribadi ---");
	System.out.println("- Mesin Mobil ini adalah : "+ mesin);
	System.out.println("- Roda mobil ini : " + roda);
	System.out.println("- Kapasitas penumpang di mobil ini adalah : " + kpenumpang);
}
}

class Main{
public static void main(String[] args){
	Bis a = new Bis();
	a.tipe(); 
	Mobil b = new Mobil();
	b.tipe();
	}
}